package br.ucsal.bes20191.cgm.calculadora;

import org.junit.Assert;
import org.junit.Test;

public class CalculadoraUtilTest {

	@Test
	public void testarSoma() {
		int a = 10;
		int b = 20;
		int somaEsperada = 30;
		int somaAtual = CalculadoraUtil.somar(a, b);
		Assert.assertEquals(somaEsperada, somaAtual);
	}

}
